<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\Employee::class, function (Faker\Generator $faker) {

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
    ];
});

$factory->define(App\Car::class, function (Faker\Generator $faker) {

    return [
        'number_plate' => $faker->unique()->regexify('[A-Z]{2}-[0-9]{4}'),
        'year' => $faker->year($min = 1970, $max = 2017),
        'make' => $faker->randomElement(['Audi','Toyota','Bugatti']),
        'model' => $faker->randomElement(['Corsa','Corola','Veyron']),
    ];
});

