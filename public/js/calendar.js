//Seting up the calendar
$(document).ready(function() {
    $('#calendar').fullCalendar({
        firstDay: 1,
        defaultView: 'listMonth',
        header: {
            left:   'title',
            right:  'today prev,next'},
        events: '/calendar_events',
        noEventsMessage: 'Šajā mēnesī nav darbiniekiem piesaistīti auto.',
        timeFormat: 'HH:mm'
    })
});