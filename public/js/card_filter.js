//extend jquery to have case insensitive contains lookup
$.extend($.expr[':'], {
    'containsi': function(elem, i, filter)
    {
        return (elem.textContent || elem.innerText || '').toLowerCase()
                .indexOf((filter[3] || "").toLowerCase()) >= 0;
    }
});

//Filter the card titles based of value in text box. Event fired when text box content changes
$('.searchbox-input').on('input propertychange', function () {

    $('.card').parent().show();
    var search = $(this).val();
    $('.cards').find(".card-title:not(:containsi(" + search + "))").parent().parent().parent().css('display','none');
});