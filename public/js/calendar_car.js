//Seting up the calendar
$(document).ready(function() {
    $('#calendar').fullCalendar({
        firstDay: 1,
        defaultView: 'agendaWeek',
        header: {
            left:   'title',
            right:  'today prev,next'},
        events: '/cars/' + window.location.href.substring(window.location.href.lastIndexOf('/') + 1),
        noEventsMessage: 'Neviens darbinieks nav piesaistīts šai mašīnai.',
        timeFormat: 'HH:mm'
    })
});