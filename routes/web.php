<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AutoController@calendar')->name('calendar');
Route::get('/{id}', 'AutoController@event')->name('event');
Route::get('/employees', 'AutoController@employees')->name('employees');
Route::get('/cars', 'AutoController@cars')->name('cars');
Route::get('/calendar_events', 'AutoController@calendarEvents')->name('calendarEvents');
Route::get('/cars/{id}', 'AutoController@carCalendarEvents')->name('carCalendarEvents');
Route::get('/car/{id}', 'AutoController@carCalendar')->name('carCalendar');
Route::get('/employees/{id}', 'AutoController@employeeCalendarEvents')->name('employeeCalendarEvents');
Route::get('/employee/{id}', 'AutoController@employeeCalendar')->name('employeeCalendar');

Route::post('/employee', 'AutoController@createEmployee')->name('createEmployee');
Route::post('/car', 'AutoController@createCar')->name('createCar');
Route::post('/event', 'AutoController@createEvent')->name('createEvent');