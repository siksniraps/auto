#Uzdevums

##Tēma:
Uzņēmums/darbinieki/automašīnas

##Uzdevums: 
Izveidot  sistēmu, kura ļauj veidot uzņēmuma auto lietošanas grafiku darbiniekiem (piesaistīt automašīnu uz uz noteiktu laiku)

##Norādījumi:
####Minimālās prasības: 
* 2 formas (viens saraksts ar darbiniekiem un automobiļiem no cikiem līdz cikiem kurš darbinieks lieto auto, darbinieku auto sasaistes forma),
* Relāciju DB pēc paša izvēles,
* Java/PHP/Ruby  utt. atbilstoši vajadzībām,
* Izdarīto iesniegt tā, lai varam novērtēt.

#Demo pieejams http://siksniraps.id.lv/

#Uzstādīšana (Windows)
1.	Uzstāda kādu AMPP serveri, piemēram, XAMPP.
2.	Uzinstalēt composer.
3.	Izveidot datubāzi priekš šī projekta.
4.	Noklonēt šo repozitoriju web root mapē.
5.	Nomainīt web root uz public mapi.
6.	composer install
7.	Izveidot .env failu pēc .env.example šablona. Galvenais norādīt informāciju par datubāzi.
8.	php artisan key:generate
9.	php artisan migrate
10.	Lai iegūtu testa datus var palaist php artisan db:seed

#Datubāze
![auto_er.jpg](https://bitbucket.org/repo/yxLk8M/images/205419605-auto_er.jpg)