<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Car extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number_plate', 'vin', 'year', 'make', 'model',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     *  Relationships
     */

    public function calendar_events(){
        return $this->hasMany('App\CalendarEvent');
    }

    public function employees(){
        return $this->hasMany('App\Employee', 'calendar_event', 'car_id', 'employee_id')->withPivot('start_at', 'end_at');
    }

}
