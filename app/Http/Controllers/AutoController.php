<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee as Employee;
use App\Car as Car;
use App\CalendarEvent as CalendarEvent;

class AutoController extends Controller
{

    //return view with calendar and car assignment form
    public function calendar(){
        $employees = Employee::all()->sortBy('first_name');
        $cars = Car::all()->sortBy('number_plate');
        return View('calendar', ['employees' => $employees, 'cars' => $cars]);
    }

    //returns view of all of the employees
    public function employees(){
        $employees = Employee::all()->sortBy('first_name');
        return View('employee_list', ['employees' => $employees]);
    }

    //returns view of all of the cars
    public function cars(){
        $cars = Car::all()->sortBy('number_plate');
        return View('cars_list', ['cars' => $cars]);
    }

    //json event feed for the calendar
    public function calendarEvents(Request $request){

        $start = $request->input('start');
        $end = $request->input('end');

        $events = CalendarEvent::where('start_at', '<', $end)->where('end_at', '>', $start)->get();

        //build the structure to convert to json
        $result = [];
        foreach($events as $event){
            $employee = $event->employee;
            $car = $event->car;
            $title = $employee->first_name.' '.$employee->last_name.' - '.$car->number_plate.' : '.$car->make.' '.$car->model;

            $sub['title'] = $title;
            $sub['start'] = $event->start_at;
            $sub['end'] = $event->end_at;
            $sub['url'] = '/'.$event->id;

            array_push($result, $sub);
        }

        return response()->json($result);
    }

    //get event object by id
    public function event($id){
        $event = CalendarEvent::findOrFail($id);
        return View('event', ['event' => $event]);
    }


    //create a new employee
    public function createEmployee(Request $request){

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:employees|email',
            'phone' => 'required',
        ]);

        Employee::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
        ]);
        return back();
    }

    //create a new car
    public function createCar(Request $request){

        $this->validate($request, [
            'number_plate' => 'required|unique:cars|',
            'year' => 'required|numeric',
            'make' => 'required',
            'model' => 'required',
        ]);

        Car::create([
            'number_plate' => $request->input('number_plate'),
            'year' => $request->input('year'),
            'make' => $request->input('make'),
            'model' => $request->input('model'),
        ]);

        return back();
    }


    //Create the calendar entry
    public function createEvent(Request $request){

        $this->validate($request, [
            'employee_id' => 'required|numeric',
            'car_id' => 'required|numeric',
            'start_at' => 'required',
            'end_at' => 'required',
        ]);

        //Check if the time slot is available
        $conflictingEvent = CalendarEvent::where('car_id', '=', $request->input('car_id'))
            ->where(function($q) use($request){
                $q->whereBetween('start_at', array($request->input('start_at'), $request->input('end_at')))
                    ->orWhereBetween('end_at', array($request->input('start_at'), $request->input('end_at')));
                $q->where('start_at', '<', $request->input('end_at'))->where('end_at', '>', $request->input('end_at'))
                    ->where('start_at', '<', $request->input('start_at'))->where('end_at', '>', $request->input('start_at'));
            })->first();
        //set up error if not available
        $error = array();
        if(!is_null($conflictingEvent)){
            array_push($error, 'This car is not available at this time.');
        } else{
            //create the event if available
            CalendarEvent::create([
                'employee_id' => $request->input('employee_id'),
                'car_id' => $request->input('car_id'),
                'start_at' => $request->input('start_at'),
                'end_at' => $request->input('end_at'),
            ]);
        }
        return back()->withErrors($error);
    }

    //events for the car calendar
    public function carCalendarEvents(Request $request, $id){
        $start = $request->input('start');
        $end = $request->input('end');
        $events = CalendarEvent::where('car_id', '=', $id)->where('start_at', '<', $end)->where('end_at', '>', $start)->get();

        //build the structure to convert to json
        $result = [];
        foreach($events as $event){
            $employee = $event->employee;
            $title = $employee->first_name.' '.$employee->last_name;

            $sub['title'] = $title;
            $sub['start'] = $event->start_at;
            $sub['end'] = $event->end_at;
            $sub['url'] = '/'.$event->id;

            array_push($result, $sub);
        }
        return response()->json($result);
    }

    //car calendar view
    public function carCalendar($id){
        $car = Car::findOrFail($id);
        $employees = Employee::all();
        return View('car_calendar', ['car' => $car, 'employees' => $employees]);
    }

    //events for the employee calendar
    public function employeeCalendarEvents(Request $request, $id){
        $start = $request->input('start');
        $end = $request->input('end');
        $events = CalendarEvent::where('employee_id', '=', $id)->where('start_at', '<', $end)->where('end_at', '>', $start)->get();

        //build the structure to convert to json
        $result = [];
        foreach($events as $event){
            $car = $event->car;
            $title = $car->number_plate.' : '.$car->make.' '.$car->model;

            $sub['title'] = $title;
            $sub['start'] = $event->start_at;
            $sub['end'] = $event->end_at;
            $sub['url'] = '/'.$event->id;

            array_push($result, $sub);
        }
        return response()->json($result);
    }

    //employee calendar view
    public function employeeCalendar($id){
        $employee = Employee::findOrFail($id);
        $cars = Car::all();
        return View('employee_calendar', ['employee' => $employee, 'cars' => $cars]);
    }


}
