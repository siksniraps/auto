

<nav class="navbar navbar-static-top navbar-toggleable-md navbar-inverse bg-primary mb-4">
    <div class="container">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/">AUTO GRAFIKS</a>


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Kalendārs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/employees">Darbinieki</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/cars">Mašīnas</a>
            </li>
        </ul>
    </div>
    </div>
</nav>

@section('js')
    @parent
    <link rel="stylesheet" href="/css/navbar.css">
    <script src="/js/header.js"></script>
@endsection