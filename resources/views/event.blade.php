@extends('master')

@section('content')

    <div class="row mb-4">
        <div class="col-md-4">
            <div class="card mb-3">
                <div class="card-block">
                    <h4>{{ 'No: '.Carbon\Carbon::parse($event->start_at)->format('d M Y H:i') }}</h4>
                    <h4>{{ 'Līdz: '.Carbon\Carbon::parse($event->end_at)->format('d M Y H:i') }}</h4>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            @include('employee_list_item', ['employee' => $event->employee])
        </div>

        <div class="col-md-4">
            @include('cars_list_item', ['car' => $event->car])
        </div>
    </div>


@endsection
