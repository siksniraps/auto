@extends('master')

@section('content')
    <div class="row mb-4">



        <div class="col-md-4 mb-2 mt-2">
            <h2 class="text-center text-uppercase">Darbinieki</h2>
        </div>

        <div class="col-md-4 mb-2 mt-2">
            <input class="searchbox-input form-control  " type="search" placeholder="Meklēt"/>
        </div>

        <div class="col-md-4 mb-2 mt-2">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_user_colapse" aria-expanded="false" aria-controls="add_user_colapse">
                Pievienot darbinieku
            </button>

        </div>



    </div>

    <div class="row mb-4">
        <div class="col-md-12">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="collapse" id="add_user_colapse">
                <div class="card card-block">
                    <form class="form-inline" action="/employee" method="post">
                        {{ csrf_field() }}
                        <label class="sr-only" for="inlineFormInput">Vārds</label>
                        <input type="text" class="form-control m-2" id="inlineFormInput" name="first_name" placeholder="Vārds">
                        <label class="sr-only" for="inlineFormInput">Uzvārds</label>
                        <input type="text" class="form-control m-2" id="inlineFormInput" name="last_name" placeholder="Uzvārds">
                        <label class="sr-only" for="inlineFormInput">E-pasts</label>
                        <input type="email" class="form-control m-2" id="inlineFormInput" name="email" placeholder="E-pasts">
                        <label class="sr-only" for="inlineFormInput">Telefons</label>
                        <input type="tel" class="form-control m-2" id="inlineFormInput" name="phone" placeholder="Telefons">
                        <input type="submit" class="btn btn-primary m-2" value="Saglabāt" >
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row cards">

            @foreach($employees as $employee)
            <div class="col-md-4">
                @include('employee_list_item', ['employee' => $employee])
            </div>
            @endforeach

    </div>
@endsection

@section('js')
    @parent
    <script src="/js/card_filter.js"></script>
@endsection