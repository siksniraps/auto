<div class="card mb-3">

    <div class="card-block">
        <h3 class="card-title"><a href="{{'/employee/'.$employee->id}}" style="color: black;">{{$employee->first_name.' '.$employee->last_name}}</a></h3>
        <p class="card-text" style="word-wrap: break-word;">

            {{'E-pasts: '.$employee->email}}
            <br>
            {{'Telefons: '.$employee->phone}}
        </p>
        <a href="{{'/employee/'.$employee->id}}" class="btn btn-primary">Kalendārs</a>
    </div>
</div>