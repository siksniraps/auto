<div class="card mb-3">

    <div class="card-block">
        <h3 class="card-title"><a style="color: black;" href="{{'/car/'.$car->id}}" >{{$car->number_plate}}</a></h3>
        <p class="card-text" style="word-wrap: break-word;">

            {{$car->make.' '.$car->model.', '.$car->year}}

        </p>
        <a href="{{'/car/'.$car->id}}" class="btn btn-primary">Kalendārs</a>
    </div>
</div>