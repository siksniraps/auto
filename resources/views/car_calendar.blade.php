@extends('master')

@section('content')
    <div class="row mb-4">
        <div class="col-md-12 text-center">
            <h2>{{$car->number_plate.' : '.$car->make.' '.$car->model}}</h2>
        </div>

    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card card-block">
                <form class="form container" action="/event" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            {{ csrf_field() }}
                            <label for="inlineFormInput">Darbinieks</label>
                            <select class="form-control mb-2" name="employee_id">
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->first_name.' '.$employee->last_name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="car_id" value="{{$car->id}}">
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="inlineFormInput">No</label>
                            <input type="datetime-local" class="form-control mb-2" id="inlineFormInput" name="start_at" placeholder="No">
                        </div>
                        <div class="col-md-6">
                            <label for="inlineFormInput">Līdz</label>
                            <input type="datetime-local" class="form-control mb-2" id="inlineFormInput" name="end_at" placeholder="Līdz">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center mt-3">
                            <input type="submit" class="btn btn-primary m-2" value="Pievienot" >
                        </div>
                    </div>


                </form>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="calendar">

            </div>
        </div>
    </div>



@endsection


@section('js')
    @parent
    <script src="/js/calendar_car.js"></script>
@endsection