@extends('master')

@section('content')
    <div class="row mb-4">

        <div class="col-md-4 mb-2 mt-2">
            <h2 class="text-center text-uppercase">Mašīnas</h2>
        </div>

        <div class="col-md-4  mb-2 mt-2">
            <input class="searchbox-input form-control" type="search" placeholder="Meklēt"/>
        </div>

        <div class="col-md-4 mb-2 mt-2">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_user_colapse" aria-expanded="false" aria-controls="add_user_colapse">
                Pievienot mašīnu
            </button>
        </div>



    </div>

    <div class="row mb-4">
        <div class="col-md-12">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="collapse" id="add_user_colapse">
                <div class="card card-block">
                    <form class="form-inline" action="/car" method="post">
                        {{ csrf_field() }}
                        <label class="sr-only" for="inlineFormInput">Numurzīme</label>
                        <input type="text" class="form-control m-2" id="inlineFormInput" name="number_plate" placeholder="Numurzīme">
                        <label class="sr-only" for="inlineFormInput">Gads</label>
                        <input type="number " class="form-control m-2" id="inlineFormInput" name="year" placeholder="Gads">
                        <label class="sr-only" for="inlineFormInput">Marka</label>
                        <input type="text" class="form-control m-2" id="inlineFormInput" name="make" placeholder="Marka">
                        <label class="sr-only" for="inlineFormInput">Modelis</label>
                        <input type="text" class="form-control m-2" id="inlineFormInput" name="model" placeholder="Modelis">
                        <input type="submit" class="btn btn-primary m-2" value="Saglabāt" >
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row cards">

        @foreach($cars as $car)
            <div class="col-md-4">
                @include('cars_list_item', ['car' => $car])
            </div>
        @endforeach

    </div>
@endsection

@section('js')
    @parent
    <script src="/js/card_filter.js"></script>
@endsection