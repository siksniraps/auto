@extends('master')

@section('content')

    <div class="row mb-4">
        <div class="col-md-12 text-center">
            <h2>{{$employee->first_name.' '.$employee->last_name}}</h2>
        </div>

    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card card-block">
                <form class="form container" action="/event" method="post">
                    <div class="row">

                        <div class="col-md-6">

                            {{ csrf_field() }}
                            <input type="hidden" name="employee_id" value="{{$employee->id}}">
                            <label for="inlineFormInput">Mašīna</label>
                            <select class="form-control mb-2" name="car_id">
                                @foreach($cars as $car)
                                    <option value="{{$car->id}}">{{$car->number_plate.' : '.$car->make.' '.$car->model}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="inlineFormInput">No</label>
                            <input type="datetime-local" class="form-control mb-2" id="inlineFormInput" name="start_at" placeholder="No">

                        </div>
                        <div class="col-md-6">
                            <label for="inlineFormInput">Līdz</label>
                            <input type="datetime-local" class="form-control mb-2" id="inlineFormInput" name="end_at" placeholder="Līdz">
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12  text-center mt-3">
                            <input type="submit" class="btn btn-primary" value="Pievienot" >
                        </div>
                    </div>


                </form>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="calendar">

            </div>
        </div>
    </div>



@endsection


@section('js')
    @parent
    <script src="/js/calendar_employee.js"></script>
@endsection